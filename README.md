# cookiecutter-cppproj

An *opinionated* [Cookiecutter](https://cookiecutter.readthedocs.io/en/1.7.2/index.html) template for C++ projects.

[[_TOC_]]



## Installing Cookiecutter

[Cookiecutter](https://cookiecutter.readthedocs.io/en/1.7.2/index.html) is a Python command line tool. The easiest way to install it is with [anaconda](https://docs.conda.io/en/latest/miniconda.html):

```sh
conda install cookiecutter
```

Or, with pip:

```sh
pip install cookiecutter
```

## Using This Template

Navigate to the directory where you want your project to be, and then run:

```sh
cookiecutter https://gitlab.com/GrinningGator/cookiecutter-cpp
```

After the first run, Cookiecutter will cache the project template locally and it can be used with:

```sh
cookiecutter cookiecutter-cpp
```

### Parameters

| Parameter | Description |
|---|---|
| `project_name` | Human-readable project name |
| `project_slug` | Used for directory name and default target names; defaults to the lower case version of the `project_name` parameter, with spaces replaced with `_` |
| `project_short_description` | A short description, will end up in `CMakeLists.txt` and `README.md` |
| `project_version` | Project version number: `<Major>.<Minor>.<Patch>` |
| `target_name` | Library/Executable name; default to `project_slug` |
| `target_type` | `Executable` (default) or `Library` |
| `cpp_std` | `17` (default), `14`, `11`, or `20` |
| `vscode` | `y`: create a `.vscode/settings.json` file; `n`: do not create a `.vscode/` directory |
| `conan_package` | `y`: create a `conanfile.py` recipe; `n` (default): create a `conanfile.txt` dependency file |
| `catch_version` | Choose a version of the [Catch2](https://github.com/catchorg/Catch2) library; Default: `2.13.8`; Can be `none` |
| `test_target_name` | Target name for unit test executable; Default: `<target_name>_tests` |


## Output Project

The output from this template is a project directory for a C++ library or executable. It uses [CMake](https://cmake.org/) to generate the build system and [Conan](https://conan.io/) to manage dependencies.

- `<project_slug>/`
    - `.vscode.`
        - `settings.json`: with [configuration options](#vscode-settings) for the VSCode [CMake Tools](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cmake-tools) extension (disable by setting `vscode` [parameter](#parameters) to `n`)
    - `cmake/`
	    - `ConanSetup.cmake`: CMake function to install Conan dependencies
        - `Version.hpp.in`: CMake template file that will create a `Version.hpp` file when the project is configured
	- `include/`
        - `<target_name>/`: Public header files; For Libraries packaged as Conan Package, headers in this directory will be exported to the package
    - `src/`
        - `main.cpp` (only if `target_type` [parameter](#parameters) is set to `Executable`)
        - `<target_name>.hpp` and `<target_name>.cpp`: skeleton files to to start the library
    - `tests/`: unit tests using the [Catch2](https://github.com/catchorg/Catch2) library
        - `CMakeLists.txt`: CMake file for unit test executable target
        - `test_main.cpp`: Main function for unit test executable with meta-tests
    - `.gitignore`: set to ignore build products and IDE configuration files
    - `CMakeLists.txt`: main CMake file, defines the library/executable target
    - `conanfile.py`: Conan recipe file (if `conan_package` is `y`)
    - `conanfile.txt`: Conan file to define dependencies (if `conan_package` is `n`)
    - `README.md`: Skeleton README file


### VSCode Settings

If the `vscode` parameter is set to `y`, Cookiecutter will create a `.vscode/settings.json` file that looks like this:

```json
{
    "C_Cpp.default.configurationProvider": "ms-vscode.cmake-tools",
    "cmake.configureSettings": {
        "CMAKE_BUILD_TYPE": "${buildType}"
    },
    "spellright.ignoreFiles": [
        "**/.gitignore",
        "**/.spellignore",
        "**/conanfile.txt",
        "**/.clang*"
    ]
}
```

The `"C_Cpp.default.configurationProvider"` key tells VSCode to use the [CMake Tools](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cmake-tools) extension.

The `"cmake.configureSettings"` sets CMake's `CMAKE_BUILD_TYPE` variable to the build type selected in VSCode (via the CMake Tools extension). This is required when using Visual Studio (or other multi-configuration) generators. With single-configuration generators (such as Make or Ninja), CMake would set the `CMAKE_BUILD_TYPE` variable automatically.

To turn on Clang Tidy, add this to the `.vscode/settings.json` file:

```json
    "C_Cpp.codeAnalysis.clangTidy.enabled": true,
    "C_Cpp.codeAnalysis.clangTidy.checks.disabled": [
        "clang-diagnostic-pragma-once-outside-header"
    ]
```

The disabled check fixes an issue where VSCode will incorrectly warn about `#pragma once` outside a header when it *is* inside a header.


### Executable Architecture

When the `target_type` [parameter](#parameters) is set to `Executable`, the main `CMakeLists.txt` will define two targets: `<target_name>` is an executable target, and `<target_name>_lib` will be a library target. The intent is that most of the functionality will be in the library target, which is linked to both the final executable target and the unit testing executable target (defined in `tests/CMakeLists.txt`). This allows for the functionality to be fully unit tested.

A typical pattern would be to define an "Application" class in the library target:

```cpp
struct Application
{
    Application(int argc, const char* argv[]);

    int Run();
};
```

And then write a very simple `main()` function in the executable target that simply constructs an Application object and calls its `Run()` function:

```cpp
int main(int argc, const char* argv[])
{
    Application app(argc, argv);

    return app.Run();
}
```

### Manually Building the Project

The resulting project structure is designed for "out of source" builds. In other words, Conan and CMake should be run inside a `build/` subdirectory.

To run the steps by hand, run the follow commands from the project directory:

```sh
mkdir build
cd build
cmake ..
cmake --build .
```

The main `CMakeLists.txt` file runs the script in `cmake/ConanSetup.cmake` which runs `conan install` during CMake's configure step. There is no need to call `conan install` manually.

The `cmake/ConanSetup.cmake` script will create `Find*.cmake` files in the `build/` subdirectory and add the `build/` directory to the `CMAKE_MODULE_PATH` variable. Simply call `find_package(...)` from the `CMakeLists.txt` script to make these targets available.


#### Building Without Unit Tests

To build the project without building the unit tests (for deployment, for example), configure CMake with the `BUILD_TESTING` option set to `OFF`:

```sh
cmake -DBUILD_TESTING=OFF ..
cmake --build .
```

### Running Unit Tests

The unit tests can be run with [CTest](https://gitlab.kitware.com/cmake/community/-/wikis/doc/ctest/Testing-With-CTest):

```sh
cd build
ctest
```

### Adding Dependencies

To add a dependency, simply edit the `conanfile.txt` and then rerun CMake to reconfigure the project. The script in `cmake/ConanSetup.cmake` will run `conan install` which will generate `Find*.cmake` scripts in the `build/` directory.

```sh
cd build
cmake ..
```

### Changing the Version Number

The project version number is set in the main `CMakeLists.txt` file, specifically in the `project()` call:

```cmake
project(<project_name>
        VERSION 0.1.0
        DESCRIPTION "..."
        LANGUAGES CXX)
```

The `CMakeLists.txt` is set up to generate `src/Version.hpp` at configuration time. So, to update the version number, update the `project()` call and re-configure the project:

```sh
cd build
cmake ..
```

## Testing the Template

From the `cookiecutter-cppproj` directory:

```sh
cookiecutter -o testing -f .
```

The `testing/` directory is in the `.gitignore` file, so by putting all the test runs there, they won't be committed to the git repo.
