import os
import shutil


def remove(filepath):
    if os.path.isfile(filepath):
        os.remove(filepath)
    elif os.path.isdir(filepath):
        shutil.rmtree(filepath)


# remove .vscode directory if user sets vscode = n
if '{{cookiecutter.vscode}}'.lower() != 'y':
    remove('.vscode')

# remove main.cpp for library projects
if '{{cookiecutter.target_type}}'.lower() != 'executable':
    remove('src/main.cpp')

# Leave only one of conanfile.txt or conanfile.py
if '{{cookiecutter.conan_package}}'.lower() == 'y':
    remove('conanfile.txt')
else:
    remove('conanfile.py')
