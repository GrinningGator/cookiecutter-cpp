{% if cookiecutter.target_type == "Executable" -%}
{% set names = namespace(lib_name=cookiecutter.target_name ~ "_lib") %}
{%- else -%}
{% set names = namespace(lib_name=cookiecutter.target_name) %}
{%- endif -%}

# Generated Files #######################################################################

# generate version header
configure_file(${PROJECT_SOURCE_DIR}/cmake/Version.hpp.in ${CMAKE_CURRENT_SOURCE_DIR}/Version.hpp)



# Library ###############################################################################
{% if cookiecutter.target_type == "Executable" -%}
# put most functionality in a library so it can be unit tested
{%- endif %}
add_library({{ names.lib_name }})

# set c++ standard and turn up warnings
target_config({{ names.lib_name }})

target_link_libraries({{ names.lib_name }}
    PUBLIC
        #...
)

target_include_directories({{ names.lib_name }}
    PUBLIC
        ${PROJECT_SOURCE_DIR}/include/
)

target_sources({{ names.lib_name }}
    PRIVATE
        {{ cookiecutter.target_name }}.cpp
    # add headers to sources so IDEs know about them
        ${PROJECT_SOURCE_DIR}/include/{{ cookiecutter.target_name }}/{{ cookiecutter.target_name }}.hpp
        Version.hpp
)


{% if cookiecutter.target_type == "Executable" -%}
# Executable ############################################################################
add_executable({{ cookiecutter.target_name }})

# set c++ standard and turn up warnings
target_config({{ cookiecutter.target_name }})

target_link_libraries({{ cookiecutter.target_name }}
    PRIVATE
        {{ names.lib_name }}
)

target_sources({{ cookiecutter.target_name }}
    PRIVATE
        main.cpp
)
{% endif %}
