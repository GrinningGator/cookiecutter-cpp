from conans import ConanFile, CMake
from conans.tools import load
import re, os


class {{ cookiecutter.target_name }}Conan(ConanFile):
    author = "YOUR NAME AND EMAIL HERE"
    url = "YOUR URL HERE"
    topics = ("YOUR", "TOPICS", "HERE")
    description = "{{ cookiecutter.project_short_description }}"
    license = "Confidential & Proprietary"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": False}
    exports_sources = "CMakeLists.txt", "src/*", "include/*", "cmake/*"
    generators = "cmake_find_package"
    {% if cookiecutter.catch_version != 'none' -%}
    build_requires = "catch2/{{ cookiecutter.catch_version }}"
    {%- endif %}
    # requires = "<package>/<version>@<user>/<channel>", "<package>/<version>@<user>/<channel>"

    def set_name(self):
        content = load(os.path.join(self.recipe_folder, "CMakeLists.txt"))
        match = re.search(r"(?:project|PROJECT)\((.*)[\s\S]*?VERSION(.*)", content)
        self.name = match.group(1).strip()

    def set_version(self):
        content = load(os.path.join(self.recipe_folder, "CMakeLists.txt"))
        match = re.search(r"(?:project|PROJECT)\((.*)[\s\S]*?VERSION(.*)", content)
        self.version = match.group(2).strip()

    def build(self):
        cmake = CMake(self)
        cmake.definitions["BUILD_TESTING"] = "OFF"
        cmake.configure(source_folder=".")
        cmake.build()

    def package(self):
        self.copy("*.h", dst="include/{{ cookiecutter.target_name }}", src="include/{{ cookiecutter.target_name }}")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["{{ cookiecutter.target_name }}"]
