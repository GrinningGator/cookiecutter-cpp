# {{ cookiecutter.project_name }}

{{ cookiecutter.project_short_description }}


## Build Instructions

From the project directory:

```sh
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=[Release|Debug] ..
cmake --build . --config [Release|Debug]
```
