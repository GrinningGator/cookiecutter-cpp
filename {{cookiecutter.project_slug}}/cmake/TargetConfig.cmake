#########################################################################################
# target_config(): Set common properties and warnings for a given target
#
# Usage:
#     target_config(<target>)
#
# Arguments (all required):
#
#     <target>: Target Name (required)
#
function(target_config TARGET)

    # set c++ standard
    set_target_properties(${TARGET}
        PROPERTIES
            CXX_STANDARD {{ cookiecutter.cpp_std }}
            CXX_STANDARD_REQUIRED YES
            CXX_EXTENSIONS NO
    )

    # set high level of warnings
    if(MSVC)
        target_compile_options(${TARGET} PRIVATE /W4 /w14640 /w14242 /w14254 /w14826 /permissive- /WX)
    else()
        target_compile_options(${TARGET} PRIVATE -Wall -Wextra -Wshadow -Wnon-virtual-dtor -Wconversion -Wsign-conversion -pedantic -Werror)
    endif()

endfunction(target_config)
